<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ahmad Nabil Alwi</title>
    <link rel="stylesheet" href="portofolio.css">  
    <script>
        function sendMessage() {
            var message = document.getElementById("messageInput").value;
            alert("Pesan terkirim: " + message);
        }
    </script>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
        <section>
            <div class="container">
                <div class="profile">
                    <img src="profil.jpeg" alt="">
                    <h2>Ahmad Nabil Alwi</h2>
                    <h4>Web Developer</h4>
                </div>
            </div>
        </section>
    </header>

    <main>
        <div class="container">
            <div class="about">
                <h2>Media Sosial</h2>
                <div class="grid-container">
                    <!-- Your existing content goes here -->
                </div>
            </div>
            <div class="alamat">
                <h2>Alamat</h2>
                <br>Gondokusuman, Jl. Sagan No. 88
                <ul>
                    <p1>
                    <br>
                </ul>
            </div>
            <div class="organitation">
                
            </div>
        </div>
    </main>

    <footer>
        <div class="container">
            <div class="contact">
                <h4>Kontak</h4>
                <p>Email: abilalwiee@example.com</p>
                <p>Phone: 0895-0631-7877</p>
                <!-- Add form to send the message -->
                <form method="post">
                    <input type="text" id="messageInput" name="messageInput" placeholder="Masukkan pesan">
                    <button type="submit" name="submit">Kirim Pesan</button>
                </form>
            </div>
        </div>
    </footer>
</body>
</html>

<?php
    if(isset($_POST['submit'])) {
        $message = $_POST['messageInput'];
        echo "<script>alert('Pesan terkirim: " . $message . "');</script>";
    }
?>
