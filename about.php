<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ahmad Nabil Alwi</title>
    <link rel="stylesheet" href="portofolio.css">  
    <script>
        function sendMessage() {
            var message = document.getElementById("messageInput").value;
            alert("Pesan terkirim: " + message);
        }
    </script>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
        <section>
            <div class="container">
                <div class="profile">
                    <img src="profil.jpeg" alt="">
                    <h2>Ahmad Nabil Alwi</h2>
                    <h4>Web Developer</h4>
                </div>
            </div>
        </section>
    </header>

    <main>
        <!-- Your existing content goes here -->
    </main>

    <footer>
    </footer>
</body>
</html>
